package edu.j4edu;

import edu.j4edu.DAO.ContactDAO;
import edu.j4edu.DAO.PhoneDAO;
import edu.j4edu.model.Contact;
import edu.j4edu.model.Phone;
import edu.j4edu.model.PhoneType;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws IOException {

//        List<Phone> phones = new ArrayList<Phone>();
//        phones.add(new Phone("+38067", PhoneType.MOBILE));
//        phones.add(new Phone("+380322", PhoneType.HOME));

//        Contact newContact = new Contact("Mike", "Helman", phones);
//        System.out.println(newContact);

//        FileOutputStream fos = new FileOutputStream("temp.out");
//        ObjectOutputStream oos = new ObjectOutputStream(fos);
//        oos.writeObject(newContact);
//        oos.flush();
//        oos.close();

//
//        test create and update contact
//        Contact testContact = new Contact("up8", "update888");
//        testContact.setContactID(new ContactDAO().insert(testContact));
//        testContact.setFirstName("updateName11");
//        testContact.setLastName("updateLastName11");
//        new ContactDAO().update(testContact);
//
//
//        test delete
//        Contact testContact = new Contact();
//        testContact.setContactID(35);
//        new ContactDAO().delete(testContact);

//
//        test geByID
//        Contact testContact = new Contact();
//        testContact.setContactID(2);
//        testContact = new ContactDAO().getById(testContact.getContactID());
//        System.out.println(testContact);


        List<Contact> contacts = new ContactDAO().getAll();
        System.out.println(" My contacts : ");
        for (Contact contact : contacts) {
            System.out.println(contact.toString());
        }


    }
}
