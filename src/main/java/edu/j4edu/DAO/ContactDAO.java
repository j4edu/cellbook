package edu.j4edu.DAO;

import edu.j4edu.model.Contact;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ContactDAO extends AbstractDAO {

    @Override
    public List getAll() {
        List<Contact> listContact = new ArrayList<Contact>();

//        Properties props = new Properties();
//        try (FileInputStream in = new FileInputStream("src/main/java/edu/j4edu/configuration/db.properties")) {
//            props.load(in);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        String driverForDB = props.getProperty("jdbc.driver");
//        String urlDB = props.getProperty("jdbc.url");
//        String usernameDB = props.getProperty("jdbc.username");
//        String passDB = props.getProperty("jdbc.password");
//        try {
//            Class.forName(driverForDB);
//        } catch (Exception e) {
//            System.out.println("JDBC driver not loaded ");
//            e.printStackTrace();
//        }
        try (Connection connection = super.getDBConection()) {
            PreparedStatement doSelect = connection.prepareStatement("select * from contact;");
            try {
                ResultSet resultSet = doSelect.executeQuery();
                while (resultSet.next()) {
                    Contact contact = new Contact();

                    contact.setContactID(resultSet.getInt("contactid"));
                    contact.setFirstName(resultSet.getString("firstname"));
                    contact.setLastName(resultSet.getString("lastname"));
                    contact.setNotes(resultSet.getString("notes"));
                    contact.setBirthday(resultSet.getDate("birthday"));

                    listContact.add(contact);
                }
            } catch (Exception e) {
                System.out.println("Query execution error ");
                e.printStackTrace();
            }
        } catch (SQLException e) {
            System.out.println("No connection with DB  ");
            e.printStackTrace();
        }
        return listContact;
    }

    @Override
    public Contact getById(long id) {
        Contact contact = new Contact();

        try (Connection connection = super.getDBConection()) {
            PreparedStatement statement = connection.prepareStatement("select * from contact where contactid = ?;");
            try {
                statement.setLong(1, id);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    contact.setFirstName(resultSet.getString("firstName"));
                    contact.setLastName(resultSet.getString("lastName"));
                    contact.setNotes(resultSet.getString("notes"));
                    contact.setBirthday(resultSet.getDate("birthday"));
                }
            } catch (Exception e) {
                System.out.println("Query execution error ");
                e.printStackTrace();
            }
        } catch (SQLException e) {
            System.out.println("No connection with DB  ");
            e.printStackTrace();
        }
        contact.setContactID(id);
        return contact;
    }

    @Override
    public long insert(Contact contact) {
        int contactID = -1;

        try (Connection connection = super.getDBConection()) {
            String sqlStr = "INSERT INTO public.contact (firstname, lastname, notes, birthday) VALUES (?, ?, ?, ?) RETURNING contactid;";
            PreparedStatement statement = connection.prepareStatement(sqlStr);
            try {
                statement.setString(1, contact.getFirstName());
                statement.setString(2, contact.getLastName());
                statement.setString(3, contact.getNotes());
                statement.setDate(4, (Date) contact.getBirthday());

                // functionality for getBack contactID after creating
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    contactID = resultSet.getInt("contactID");
                    System.out.println("Contact is created, ID = " + contactID);
                }
            } catch (Exception e) {
                System.out.println("Query execution error ");
                e.printStackTrace();
            }
        } catch (SQLException e) {
            System.out.println("No connection with DB  ");
            e.printStackTrace();
        }
        return contactID;
    }

    @Override
    public void update(Contact contact) {

        try (Connection connection = super.getDBConection()) {
            String sqlStr = "UPDATE public.contact SET firstname=?, lastname=?, notes=?, birthday=? WHERE contactid=?;";
            PreparedStatement statement = connection.prepareStatement(sqlStr);
            try {
                statement.setString(1, contact.getFirstName());
                statement.setString(2, contact.getLastName());
                statement.setString(3, contact.getNotes());
                statement.setDate(4, (Date) contact.getBirthday());
                statement.setLong(5, contact.getContactID());
                if (statement.execute()) {
                    System.out.println("Entity updated ");
                }
            } catch (Exception e) {
                System.out.println("Query execution error ");
                e.printStackTrace();
            }
        } catch (Exception e) {
            System.out.println("No connection with DB  ");
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Contact contact) {
        try (Connection connection = super.getDBConection()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM public.contact WHERE contactid=?;");
            try {
                statement.setLong(1, contact.getContactID());
                if (statement.execute()) {
                    System.out.println("Entity deleted ");
                }
            } catch (Exception e) {
                System.out.println("Query execution error ");
                e.printStackTrace();
            }
        } catch (Exception e) {
            System.out.println("No connection with DB  ");
            e.printStackTrace();
        }
    }

}
