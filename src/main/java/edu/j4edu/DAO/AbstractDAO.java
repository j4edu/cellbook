package edu.j4edu.DAO;

import edu.j4edu.model.Contact;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public abstract class AbstractDAO<T> {

    public abstract T getById(long id);

    public abstract void update(Contact contact);

    public abstract void delete(Contact contact);

    public abstract List<T> getAll();

    public abstract long insert(Contact contact);

    public Connection getDBConection() {

        Properties props = new Properties();
        try (FileInputStream in = new FileInputStream("src/main/java/edu/j4edu/configuration/db.properties")) {
            props.load(in);
        } catch (FileNotFoundException e) {
            System.out.println("Properties file with parameters for DB connection not found ");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String driverForDB = props.getProperty("jdbc.driver");
        String urlDB = props.getProperty("jdbc.url");
        String usernameDB = props.getProperty("jdbc.username");
        String passDB = props.getProperty("jdbc.password");

        try {
            Class.forName(driverForDB);
        } catch (Exception e) {
            System.out.println("JDBC driver not loaded ");
            e.printStackTrace();
        }

        Connection connectionToDB = null;
        try {
            connectionToDB = DriverManager.getConnection(urlDB, usernameDB, passDB);
            System.out.println("Opened database successfully");
            return connectionToDB;
        } catch (SQLException e) {
            System.out.println("No connection with DB  ");
            e.printStackTrace();
        }
        return null;
    }

    public void closeDBConection(Connection connection) {
        try {
            connection.close();
        } catch (Exception e) {
            System.out.println("Connection with DB not close ");
            e.printStackTrace();
        }
    }

}


