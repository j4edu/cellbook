package edu.j4edu.model;

public enum EmailType {
    PERSONAL, WORK, OTHER
}
