package edu.j4edu.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Contact implements Serializable {

    private long contactID;
    private String firstName;
    private String lastName;
    private String notes;
    private Date birthday;
    private List<Phone> phones;
    private List<Email> emails;
    private List<Address> addresses;
    private List<SocialContact> socialContacts;

    public long getContactID() {
        return contactID;
    }

    public void setContactID(long contactID) {
        this.contactID = contactID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }


    public Contact(String firstName, String lastName, String notes, Date birthday, List<Phone> phones, List<Email> emails, List<Address> addresses, List<SocialContact> socialContacts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.notes = notes;
        this.birthday = birthday;
        this.phones = phones;
        this.emails = emails;
        this.addresses = addresses;
        this.socialContacts = socialContacts;
    }

    public Contact(String firstName, String lastName, List<Phone> phones) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phones = phones;
    }

    public Contact(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Contact() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return Objects.equals(getFirstName(), contact.getFirstName()) &&
                Objects.equals(getLastName(), contact.getLastName()) &&
                Objects.equals(getNotes(), contact.getNotes()) &&
                Objects.equals(getBirthday(), contact.getBirthday()) &&
                Objects.equals(phones, contact.phones) &&
                Objects.equals(emails, contact.emails) &&
                Objects.equals(addresses, contact.addresses) &&
                Objects.equals(socialContacts, contact.socialContacts);
    }

    @Override
    public int hashCode() {

        return Objects.hash(getFirstName(), getLastName(), getNotes(), getBirthday(), phones, emails, addresses, socialContacts);
    }

    @Override
    public String toString() {
        return "Contact{" +
                "ID='" + contactID + '\'' +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", notes='" + notes + '\'' +
                ", birthday=" + birthday +
                ", phones=" + phones +
                ", emails=" + emails +
                ", addresses=" + addresses +
                ", socialContacts=" + socialContacts +
                '}';
    }


}
