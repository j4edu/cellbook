package edu.j4edu.model;

import java.io.Serializable;
import java.net.URL;
import java.util.Objects;

public class SocialContact implements Serializable {

    private long id;
    private URL link;
    private SocialContactType type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public URL getLink() {
        return link;
    }

    public void setLink(URL link) {
        this.link = link;
    }

    public SocialContactType getType() {
        return type;
    }

    public void setType(SocialContactType type) {
        this.type = type;
    }

    public SocialContact(URL link, SocialContactType type) {
        this.link = link;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SocialContact that = (SocialContact) o;
        return Objects.equals(getLink(), that.getLink()) &&
                getType() == that.getType();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getLink(), getType());
    }

    @Override
    public String toString() {
        return "Social{" +
                "id='" + id + '\'' +
                "link=" + link +
                ", type='" + type + '\'' +
                '}';
    }
}
