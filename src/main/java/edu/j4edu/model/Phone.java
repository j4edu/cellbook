package edu.j4edu.model;

import java.io.Serializable;
import java.util.Objects;

public class Phone implements Serializable {

    private long id;
    private String telephoneNumber;
    private PhoneType type;

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Phone(String telephoneNumber, PhoneType type) {
        this.telephoneNumber = telephoneNumber;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return Objects.equals(getTelephoneNumber(), phone.getTelephoneNumber()) &&
                getType() == phone.getType();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getTelephoneNumber(), getType());
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id='" + id + '\'' +
                "telephoneNumber='" + telephoneNumber + '\'' +
                ", type=" + type +
                '}';
    }
}
