package edu.j4edu.model;

public enum SocialContactType {
    FB, GOOGLE, INSTAGRAM, OTHER
}
