package edu.j4edu.model;

import java.io.Serializable;
import java.util.Objects;

public class Email implements Serializable {

    private long id;
    private String address;
    private EmailType type;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public EmailType getType() {
        return type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setType(EmailType type) {
        this.type = type;
    }

    public Email(String address, EmailType type) {
        this.address = address;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email email = (Email) o;
        return Objects.equals(getAddress(), email.getAddress()) &&
                getType() == email.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAddress(), getType());
    }

    @Override
    public String toString() {
        return "Email{" +
                "id='" + id + '\'' +
                "address='" + address + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
