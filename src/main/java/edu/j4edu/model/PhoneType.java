package edu.j4edu.model;

public enum PhoneType {
    MOBILE, HOME, WORK, FAX, OTHER
}
